import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Rating, AirbnbRating} from 'react-native-ratings';

const CardMovie = ({data, navigation}) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        onPress={() => navigation.push('MovieDetails', {id: data.id})}>
        <ImageBackground
          source={{uri: `https://image.tmdb.org/t/p/w500${data.poster_path}`}}
          style={styles.poster}>
          <View style={styles.genreView}>
            <Text style={styles.genre} adjustsFontSizeToFit>
              {data.genre_ids[0] == 28
                ? 'Action'
                : data.genre_ids[0] == 12
                ? 'Adventure'
                : data.genre_ids[0] == 16
                ? 'Animation'
                : data.genre_ids[0] == 35
                ? 'Comedy'
                : data.genre_ids[0] == 80
                ? 'Crime'
                : data.genre_ids[0] == 99
                ? 'Documentary'
                : data.genre_ids[0] == 18
                ? 'Drama'
                : data.genre_ids[0] == 10751
                ? 'Family'
                : data.genre_ids[0] == 14
                ? 'Fantasy'
                : data.genre_ids[0] == 36
                ? 'History'
                : data.genre_ids[0] == 27
                ? 'Horror'
                : data.genre_ids[0] == 10402
                ? 'Music'
                : data.genre_ids[0] == 9648
                ? 'Mystery'
                : data.genre_ids[0] == 10749
                ? 'Romance'
                : data.genre_ids[0] == 878
                ? 'Science Fiction'
                : data.genre_ids[0] == 10770
                ? 'TV Movie'
                : data.genre_ids[0] == 53
                ? 'Thriller'
                : data.genre_ids[0] == 10752
                ? 'War'
                : data.genre_ids[0] == 37
                ? 'Western'
                : 'Others'}
            </Text>
          </View>
          <AirbnbRating
            showRating={false}
            isDisabled={true}
            size={12}
            defaultRating={data.vote_average}
            count={10}
            selectedColor="#FFFFFF"
            starContainerStyle={{alignSelf: 'flex-start'}}
          />
          <Text style={styles.title}>{data.title}</Text>
        </ImageBackground>
      </TouchableOpacity>
    </View>
  );
};

export default CardMovie;

const styles = StyleSheet.create({
  container: {
    width: wp('58%'),
    height: hp('38%'),
    borderRadius: 4,
    elevation: 3,
    overflow: 'hidden',
    marginLeft: wp('4%'),
    marginVertical: hp('2%'),
  },
  poster: {
    width: wp('58%'),
    height: hp('38%'),
    resizeMode: 'contain',
    justifyContent: 'flex-end',
    paddingLeft: wp('4%'),
    paddingVertical: hp('1%'),
  },
  title: {
    fontSize: hp('2.5%'),
    fontFamily: 'Roboto-Bold',
    color: '#FFFFFF',
  },
  genreView: {
    borderTopRightRadius: 10,
    borderBottomLeftRadius: 10,
    alignSelf: 'flex-start',
    backgroundColor: 'rgba(15, 239, 253, 0.1)',
  },
  genre: {
    fontSize: hp('2%'),
    fontFamily: 'Roboto-Regular',
    color: '#0FEFFD',
    paddingHorizontal: wp('1.8%'),
    paddingVertical: hp('0.5%'),
  },
});
